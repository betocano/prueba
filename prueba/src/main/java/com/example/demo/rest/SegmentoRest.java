package com.example.demo.rest;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.DAO.SegmentoDAO;
import com.example.demo.model.Segmento;

@RestController
@RequestMapping("segmento")
@CrossOrigin("*")
public class SegmentoRest {
	@Autowired
	private SegmentoDAO segmentoDAO;
	
	@PostMapping("/agregar")
	public ResponseEntity<Segmento> save(@RequestBody Segmento segmento){
		Segmento obj = segmentoDAO.save(segmento);
		return new ResponseEntity<Segmento>(obj, HttpStatus.CREATED);
	}
	
	@GetMapping("/mostrar")
	public List<Segmento> mostrar(){
		return segmentoDAO.findAll();
	}
	
	@DeleteMapping("/eliminar/{id}")
	public ResponseEntity<Void> eliminar (@PathVariable Long id){
			segmentoDAO.deleteById(id);
			return new ResponseEntity<Void>(HttpStatus.OK);
		
	}
	@PutMapping("/actualizar")
	public ResponseEntity<Segmento> editar( @RequestBody Segmento segmento){
		Segmento obj = segmentoDAO.save(segmento);
		return new ResponseEntity<Segmento>(obj,HttpStatus.OK);
	}
}
