package com.example.demo.DAO;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.model.Segmento;

public interface SegmentoDAO extends JpaRepository<Segmento, Long> {
	@Query("select s from Segmento s where s.longitud = :longitud")
	List<Segmento> getLongitud(@Param("longitud")Float longitud);

}

